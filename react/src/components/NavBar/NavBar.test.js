import React from "react";
import ReactDOM from "react-dom";
import NavBar from "./NavBar";
import { MemoryRouter as Router } from "react-router-dom";
import { act } from "react-dom/test-utils";
import { render, unmountComponentAtNode } from "react-dom";

describe("ChampionList", () => {
  it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(
      <Router>
        <NavBar />
      </Router>,
      div
    );
    unmountComponentAtNode(div);
  });

  it("should toggle the active state after a click", () => {
    const container = document.createElement("div");
    document.body.appendChild(container);

    act(() => {
      render(
        <Router>
          <NavBar />
        </Router>,
        container
      );
    });

    const activeClassName = "is-active";
    const burger = container.querySelector(".navbar-burger");
    const menu = container.querySelector(".navbar-menu");

    expect(burger.className.includes(activeClassName)).toBe(false);
    expect(menu.className.includes(activeClassName)).toBe(false);

    act(() => {
      burger.dispatchEvent(new MouseEvent("click", { bubbles: true }));
    });

    expect(burger.className.includes(activeClassName)).toBe(true);
    expect(menu.className.includes(activeClassName)).toBe(true);

    act(() => {
      burger.dispatchEvent(new MouseEvent("click", { bubbles: true }));
    });

    expect(burger.className.includes(activeClassName)).toBe(false);
    expect(menu.className.includes(activeClassName)).toBe(false);

    unmountComponentAtNode(container);
    container.remove();
  });
});
