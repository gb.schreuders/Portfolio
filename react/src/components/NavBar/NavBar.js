import React, { Component } from "react";
import { Link } from "react-router-dom";

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isActive: false
    };

    this.toggleIsActive = this.toggleIsActive.bind(this);
  }

  toggleIsActive() {
    this.setState(state => ({
      isActive: !state.isActive
    }));
  }

  render() {
    const isActiveClass = this.state.isActive ? "is-active" : "";

    return (
      <nav
        className="navbar is-primary is-fixed-top"
        role="navigation"
        aria-label="main navigation"
      >
        <div className="navbar-brand">
          <Link className="navbar-item" to="/">
            LOLINDEX
          </Link>

          {/*eslint-disable-next-line*/}
          <a
            onClick={this.toggleIsActive}
            role="button"
            className={"navbar-burger burger " + isActiveClass}
            aria-label="menu"
            aria-expanded="false"
            data-target="navbarBasicExample"
          >
            <span aria-hidden="true" />
            <span aria-hidden="true" />
            <span aria-hidden="true" />
          </a>
        </div>

        <div id="navbarBasicExample" className={"navbar-menu " + isActiveClass}>
          <div className="navbar-start">
            <Link className="navbar-item" to="/">
              Home
            </Link>

            <Link className="navbar-item" to="/champions">
              Champions
            </Link>
          </div>
        </div>
      </nav>
    );
  }
}

export default NavBar;
