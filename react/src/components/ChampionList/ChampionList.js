import React, { Component } from "react";
import getChampions from "../../services/getChampions";
import { Link } from "react-router-dom";

class ChampionList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchValue: null
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ searchValue: event.target.value });
  }

  render() {
    const rows = getChampions()
      .filter(x => {
        if (!this.state.searchValue) {
          return true;
        }

        return x.name
          .toLowerCase()
          .includes(this.state.searchValue.toLowerCase().trim());
      })
      .map(x => (
        <tr key={x.id}>
          <td>
            <figure className="image is-32x32">
              <img
                alt={x.name}
                src={
                  "https://ddragon.leagueoflegends.com/cdn/9.15.1/img/champion/" +
                  x.image.full
                }
              />
            </figure>
          </td>
          <td>
            <Link to={"/champion/" + x.name}>{x.name}</Link>
          </td>
        </tr>
      ));

    return (
      <div>
        <div className="control">
          <input
            className="input"
            type="text"
            placeholder="Champion name"
            value={this.searchValue}
            onChange={this.handleChange}
          />
        </div>

        <table className="table is-hoverable is-fullwidth">
          <thead>
            <tr>
              <th></th>
              <th>Name</th>
            </tr>
          </thead>
          <tbody>{rows}</tbody>
        </table>
      </div>
    );
  }
}

export default ChampionList;
