import React from "react";
import ReactDOM from "react-dom";
import ChampionList from "./ChampionList";
import { MemoryRouter as Router } from "react-router-dom";
import { fireEvent, render } from "@testing-library/react";

describe("ChampionList", () => {
  it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(
      <Router>
        <ChampionList />
      </Router>,
      div
    );
    ReactDOM.unmountComponentAtNode(div);
  });

  it("should filter the correct champions", () => {
    const { container } = render(
      <Router>
        <ChampionList />
      </Router>
    );

    const championListPre = container.querySelectorAll("tbody > tr");

    expect(championListPre.length).toBeGreaterThan(1);

    const search = container.querySelector("input");

    fireEvent.change(search, { target: { value: "aatrox" } });

    expect(search.value).toBe("aatrox");

    const championListPost = container.querySelectorAll("tbody > tr");

    expect(championListPost.length).toBe(1);
  });
});
