import React from "react";
import "./App.css";

import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from "./pages/Home";
import Champion from "./pages/Champion/Champion";
import Champions from "./pages/Champions";

function App() {
  return (
    <Router>
      <Route path="/" exact component={Home} />
      <Route path="/champion/:name" exact component={Champion} />
      <Route path="/champions" exact component={Champions} />
    </Router>
  );
}

export default App;
