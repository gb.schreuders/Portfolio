import champions from "../assets/champion";

export default function getChampions() {
  return Object.values(champions.data);
}
