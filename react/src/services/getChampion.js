import getChampions from "./getChampions";

export default function getChampion(name) {
  return getChampions().find(x => x.name === name);
}
