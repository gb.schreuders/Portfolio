import React, { Component } from "react";

export class Info extends Component {
  render() {
    return (
      <div className="card">
        <header className="card-header">
          <p className="card-header-title">Info</p>
        </header>
        <div className="card-content">
          <table className="table is-fullwidth is-hoverable">
            <tbody>
              <tr>
                <td>Attack</td>
                <td>{this.props.champion.info.attack}</td>
              </tr>
              <tr>
                <td>Defense</td>
                <td>{this.props.champion.info.defense}</td>
              </tr>
              <tr>
                <td>Magic</td>
                <td>{this.props.champion.info.magic}</td>
              </tr>
              <tr>
                <td>Difficulty</td>
                <td>{this.props.champion.info.difficulty}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
