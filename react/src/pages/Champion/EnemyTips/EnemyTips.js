import React, { Component } from "react";

export class EnemyTips extends Component {
  render() {
    return (
      <div className="card">
        <header className="card-header">
          <p className="card-header-title">Enemy tips</p>
        </header>
        <div className="card-content">
          <div className="content">
            <ul className="mt-0">{this.props.enemytips}</ul>
          </div>
        </div>
      </div>
    );
  }
}
