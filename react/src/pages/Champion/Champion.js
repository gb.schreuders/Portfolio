import React, { Component } from "react";
import getChampion from "../../services/getChampion";
import NavBar from "../../components/NavBar/NavBar";
import "./Champion.scss";
import { Info } from "./Info/Info";
import { EnemyTips } from "./EnemyTips/EnemyTips";
import { AllyTips } from "./AllyTips/AllyTips";
import { Lore } from "./Lore/Lore";
import { Stats } from "./Stats/Stats";

class Champion extends Component {
  render() {
    const championName = this.props.match.params.name;
    const champion = getChampion(championName);
    const allytips = champion.allytips.map((tip, index) => (
      <li key={index}>{tip}</li>
    ));
    const enemytips = champion.enemytips.map((tip, index) => (
      <li key={index}>{tip}</li>
    ));

    return (
      <div>
        <NavBar />

        <div className="section">
          <div className="container">
            <div className="columns is-mobile">
              <div className="column is-narrow">
                <h1 className="title">{champion.name}</h1>
                <h2 className="subtitle">{champion.title}</h2>
              </div>

              <div className="column">
                <figure className="image is-64x64">
                  <img
                    alt={champion.name}
                    src={
                      "https://ddragon.leagueoflegends.com/cdn/9.15.1/img/champion/" +
                      champion.image.full
                    }
                  />
                </figure>
              </div>
            </div>

            <div className="columns is-multiline">
              <div className="column is-12">
                <Lore champion={champion} />
              </div>
              <div className="column">
                <Info champion={champion} />
              </div>
              <div className="column is-6">
                <EnemyTips enemytips={enemytips} />
              </div>
              <div className="column is-6">
                <AllyTips allytips={allytips} />
              </div>
              <div className="column is-6">
                <Stats champion={champion} />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Champion;
