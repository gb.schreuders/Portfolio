import React, { Component } from "react";
import * as PropTypes from "prop-types";

export class Stats extends Component {
  render() {
    return (
      <div className="card">
        <header className="card-header">
          <p className="card-header-title">Stats</p>
        </header>
        <div className="card-content">
          <table className="table is-fullwidth is-hoverable">
            <tbody>
              <tr>
                <td>HP</td>
                <td>{this.props.champion.stats.hp}</td>
              </tr>
              <tr>
                <td>HP per level</td>
                <td>{this.props.champion.stats.hpperlevel}</td>
              </tr>
              <tr>
                <td>MP</td>
                <td>{this.props.champion.stats.mp}</td>
              </tr>
              <tr>
                <td>MP per level</td>
                <td>{this.props.champion.stats.mpperlevel}</td>
              </tr>
              <tr>
                <td>Move speed</td>
                <td>{this.props.champion.stats.movespeed}</td>
              </tr>
              <tr>
                <td>Armor</td>
                <td>{this.props.champion.stats.armor}</td>
              </tr>
              <tr>
                <td>Armor per level</td>
                <td>{this.props.champion.stats.armorperlevel}</td>
              </tr>
              <tr>
                <td>Spell block</td>
                <td>{this.props.champion.stats.spellblock}</td>
              </tr>
              <tr>
                <td>Spell block per level</td>
                <td>{this.props.champion.stats.spellblockperlevel}</td>
              </tr>
              <tr>
                <td>Attack range</td>
                <td>{this.props.champion.stats.attackrange}</td>
              </tr>
              <tr>
                <td>HP regen</td>
                <td>{this.props.champion.stats.hpregen}</td>
              </tr>
              <tr>
                <td>HP regen per level</td>
                <td>{this.props.champion.stats.hpregenperlevel}</td>
              </tr>
              <tr>
                <td>MP regen</td>
                <td>{this.props.champion.stats.mpregen}</td>
              </tr>
              <tr>
                <td>MP regen per level</td>
                <td>{this.props.champion.stats.mpregenperlevel}</td>
              </tr>
              <tr>
                <td>Crit</td>
                <td>{this.props.champion.stats.crit}</td>
              </tr>
              <tr>
                <td>Crit per level</td>
                <td>{this.props.champion.stats.critperlevel}</td>
              </tr>
              <tr>
                <td>Attack damage</td>
                <td>{this.props.champion.stats.attackdamage}</td>
              </tr>
              <tr>
                <td>Attack damage per level</td>
                <td>{this.props.champion.stats.attackdamageperlevel}</td>
              </tr>
              <tr>
                <td>Attack speed per level</td>
                <td>{this.props.champion.stats.attackspeedperlevel}</td>
              </tr>
              <tr>
                <td>Attack speed</td>
                <td>{this.props.champion.stats.attackspeed}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

Stats.propTypes = { champion: PropTypes.any };
