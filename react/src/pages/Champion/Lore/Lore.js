import React, { Component } from "react";

export class Lore extends Component {
  render() {
    return (
      <div className="card">
        <header className="card-header">
          <p className="card-header-title">Lore</p>
        </header>
        <div className="card-content">
          <div className="content">
            <p>{this.props.champion.lore}</p>
          </div>
        </div>
      </div>
    );
  }
}
