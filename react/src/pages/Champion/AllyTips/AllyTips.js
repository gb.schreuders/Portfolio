import React, { Component } from "react";

export class AllyTips extends Component {
  render() {
    return (
      <div className="card">
        <header className="card-header">
          <p className="card-header-title">Ally tips</p>
        </header>
        <div className="card-content">
          <div className="content">
            <ul className="mt-0">{this.props.allytips}</ul>
          </div>
        </div>
      </div>
    );
  }
}
