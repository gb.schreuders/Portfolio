import React from "react";
import NavBar from "../components/NavBar/NavBar";
import ChampionList from "../components/ChampionList/ChampionList";

function Home() {
  return (
    <div>
      <NavBar />

      <section className="hero is-primary">
        <div className="hero-body">
          <div className="container">
            <h1 className="title">Welcome to lolindex</h1>
            <h2 className="subtitle">Find all your champion data here</h2>
          </div>
        </div>
      </section>

      <div className="section">
        <div className="container">
          <ChampionList />
        </div>
      </div>
    </div>
  );
}

export default Home;
