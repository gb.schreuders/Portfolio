import React from "react";
import NavBar from "../components/NavBar/NavBar";
import ChampionList from "../components/ChampionList/ChampionList";

function Champions() {
  return (
    <div>
      <NavBar />

      <div className="section">
        <div className="container">
          <ChampionList />
        </div>
      </div>
    </div>
  );
}

export default Champions;
