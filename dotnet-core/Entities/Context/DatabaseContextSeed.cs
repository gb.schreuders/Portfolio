﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Entities.Context
{
    public static class DatabaseContextSeed
    {
        public static void ApplySeed(this ModelBuilder modelBuilder)
        {
            List<JsonArtist> jsonArtists;

            using (var r = new StreamReader("../Entities/Resources/artists.json"))
            {
                var json = r.ReadToEnd();
                jsonArtists = JsonConvert.DeserializeObject<List<JsonArtist>>(json);
            }

            var artists = jsonArtists.Select(a => new Artist
            {
                ArtistId = Guid.NewGuid(),
                Name = a.Name,
            }).ToArray();

            modelBuilder.Entity<Artist>().HasData(artists);

            List<JsonSong> jsonSongs;

            using (var r = new StreamReader("../Entities/Resources/songs.json"))
            {
                var json = r.ReadToEnd();
                jsonSongs = JsonConvert.DeserializeObject<List<JsonSong>>(json);
            }

            object[] songs = jsonSongs.Select(s =>
            {
                var artist = artists.FirstOrDefault(a => a.Name == s.Artist);

                if (artist == null)
                {
                    artist = new Artist
                    {
                        ArtistId = Guid.NewGuid(),
                        Name = s.Artist
                    };
                    modelBuilder.Entity<Artist>().HasData(artist);
                }

                return new
                {
                    SongId = Guid.NewGuid(),
                    ArtistId = artist.ArtistId,
                    Name = s.Name,
                    Album = s.Album ?? "",
                    Genre = s.Genre,
                    Bpm = s.Bpm ?? 0,
                    SpotifyId = s.SpotifyId ?? "",
                    Shortname = s.Shortname,
                    Year = s.Year,
                    Durration = s.Durration
                };
            }).ToArray();

            modelBuilder.Entity<Song>().HasData(songs);
        }

        private struct JsonArtist
        {
            public int Id { get; set; }

            public string Name { get; set; }
        }

        private struct JsonSong
        {
            public int Id { get; set; }

            public string Name { get; set; }

            public int Year { get; set; }

            public string Artist { get; set; }

            public string Shortname { get; set; }

            public int? Bpm { get; set; }

            public int Durration { get; set; }

            public string Genre { get; set; }

            public string SpotifyId { get; set; }

            public string Album { get; set; }
        }
    }
}