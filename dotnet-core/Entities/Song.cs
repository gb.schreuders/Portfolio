﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Entities
{
    public class Song
    {
        [Key] public Guid SongId { get; set; }

        [Required] public string Name { get; set; }

        [Required] public int Year { get; set; }

        [Required] public Artist Artist { get; set; }

        [Required] public string Shortname { get; set; }

        [Required] public int? Bpm { get; set; }

        [Required] public int Durration { get; set; }

        [Required] public string Genre { get; set; }

        [Required] public string SpotifyId { get; set; }

        [Required] public string Album { get; set; }
    }
}