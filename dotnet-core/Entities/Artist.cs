﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entities
{
    public class Artist
    {
        [Key]
        public Guid ArtistId { get; set; }

        [Required]
        public string Name { get; set; }

        public IList<Song> Songs { get; set; }
    }
}
