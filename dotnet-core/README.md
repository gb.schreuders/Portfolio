# Intro
This is an .NET core example. The project consists of a public API which can be used to CRUD music and artists.

The solution consists of different projects. Those projects are:

## Api
The main/startup project. This is the communication layer which is publicly exposed though OpenAPI (Swagger) using .NET MVC. This project contains:

- All startup configurations
- OpenAPI registery
- Controllers
- Handler registery

## Contracts.V1
This contains the requests and responses for the API calls.

## Handlers
Contains all application logic. This is where data gets collected or processed.

## Handlers.Test
Unit tests for the Handlers project.

## Entities
Almost all database related stuff goes here. Currently it contains:

- Database entities
- Database seed script
- Seed source

## Repositories
The services that talk with the database. Used by the handlers.