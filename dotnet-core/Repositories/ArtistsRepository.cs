﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entities;

namespace Repositories
{
    public class ArtistsRepository : IArtistsRepository
    {
        public Artist GetArtist(Guid id)
        {
            using (var db = new DatabaseContext())
            {
                var artist = db.Artists.FirstOrDefault(x => x.ArtistId == id);
                return artist;
            }
        }

        public Artist GetArtist(string name)
        {
            using (var db = new DatabaseContext())
            {
                var artist = db.Artists.FirstOrDefault(x => x.Name == name);
                return artist;
            }
        }

        public IList<Artist> GetArtists()
        {
            using (var db = new DatabaseContext())
            {
                var artists = db.Artists.ToList();
                return artists;
            }
        }

        public void AddArtist(string name)
        {
            using (var db = new DatabaseContext())
            {
                db.Artists.Add(new Artist
                {
                    Name = name
                });

                db.SaveChanges();
            }
        }

        public void UpdateArtist(Artist artist)
        {
            using (var db = new DatabaseContext())
            {
                db.Artists.Update(artist);
                db.SaveChanges();
            }
        }

        public void DeleteArtist(Artist artist)
        {
            using (var db = new DatabaseContext())
            {
                db.Artists.Remove(artist);
                db.SaveChanges();
            }
        }
    }
}