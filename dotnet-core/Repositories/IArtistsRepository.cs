﻿using System;
using System.Collections.Generic;
using Entities;

namespace Repositories
{
    public interface IArtistsRepository
    {
        Artist GetArtist(Guid id);
        Artist GetArtist(string name);
        IList<Artist> GetArtists();
        void AddArtist(string name);
        void UpdateArtist(Artist artist);
        void DeleteArtist(Artist artist);
    }
}