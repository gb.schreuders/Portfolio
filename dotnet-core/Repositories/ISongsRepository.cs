﻿using System;
using System.Collections.Generic;
using Entities;

namespace Repositories
{
    public interface ISongsRepository
    {
        Song GetSong(Guid id);
        Song GetSong(string name);
        IList<Song> GetSongsByGenre(string genre);
        void AddSong(Song song);
        void UpdateSong(Song song);
        void DeleteSong(Song song);
    }
}