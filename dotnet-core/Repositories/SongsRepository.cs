﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entities;
using Microsoft.EntityFrameworkCore;

namespace Repositories
{
    public class SongsRepository : ISongsRepository
    {
        public Song GetSong(Guid id)
        {
            using (var db = new DatabaseContext())
            {
                return db.Songs.Include(s => s.Artist).FirstOrDefault(x => x.SongId == id);
            }
        }

        public Song GetSong(string name)
        {
            using (var db = new DatabaseContext())
            {
                return db.Songs.Include(s => s.Artist).FirstOrDefault(x => x.Name == name);
            }
        }

        public IList<Song> GetSongsByGenre(string genre)
        {
            using (var db = new DatabaseContext())
            {
                return db.Songs.Include(s => s.Artist).Where(s => s.Genre == genre).ToList();
            }
        }

        public void AddSong(Song song)
        {
            using (var db = new DatabaseContext())
            {
                db.Songs.Attach(song);
                db.SaveChanges();
            }
        }

        public void UpdateSong(Song song)
        {
            using (var db = new DatabaseContext())
            {
                db.Songs.Update(song);
                db.SaveChanges();
            }
        }

        public void DeleteSong(Song song)
        {
            using (var db = new DatabaseContext())
            {
                db.Songs.Remove(song);
                db.SaveChanges();
            }
        }
    }
}