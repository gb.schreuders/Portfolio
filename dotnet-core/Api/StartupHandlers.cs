﻿using Handlers;
using Microsoft.Extensions.DependencyInjection;

namespace Api
{
    public static class StartupHandlers
    {
        public static void SetupHandlers(this IServiceCollection services)
        {
            services.Scan(scan => scan
                .FromAssembliesOf(typeof(IHandler<,>), typeof(DeleteArtistHandler))
                .AddClasses()
                .AsImplementedInterfaces()
            );
        }
    }
}