﻿using System;
using Contracts.V1;
using Handlers;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ArtistsController : ControllerBase
    {
        [HttpGet]
        public GetArtistsResponse Get(
            [FromQuery] GetArtistsRequest request,
            [FromServices] IHandler<GetArtistsRequest, GetArtistsResponse> handler
        )
        {
            return handler.Handle(request);
        }

        [HttpPost("{Name}")]
        public PostArtistResponse Post(
            [FromRoute] PostArtistRequest request,
            [FromServices] IHandler<PostArtistRequest, PostArtistResponse> handler
        )
        {
            return handler.Handle(request);
        }

        [HttpPut("{Id}")]
        public PutArtistResponse Put(
            Guid Id,
            PutArtistRequest request,
            [FromServices] IHandler<PutArtistRequest, PutArtistResponse> handler
        )
        {
            request.Id = Id;
            return handler.Handle(request);
        }

        [HttpDelete("{Id}")]
        public DeleteArtistResponse Delete(
            [FromRoute] DeleteArtistRequest request,
            [FromServices] IHandler<DeleteArtistRequest, DeleteArtistResponse> handler
        )
        {
            return handler.Handle(request);
        }
    }
}