﻿using System;
using Contracts.V1;
using Handlers;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class SongsController : ControllerBase
    {
        [HttpGet]
        public GetSongsResponse Get(
            [FromQuery] GetSongsRequest request,
            [FromServices] IHandler<GetSongsRequest, GetSongsResponse> handler)
        {
            return handler.Handle(request);
        }

        [HttpPost("{Name}")]
        public PostSongResponse Post(
            string Name,
            [FromBody] PostSongRequest request,
            [FromServices] IHandler<PostSongRequest, PostSongResponse> handler
        )
        {
            request.Name = Name;
            return handler.Handle(request);
        }

        [HttpPut("{Id}")]
        public PutSongResponse Put(
            Guid Id,
            [FromBody] PutSongRequest request,
            [FromServices] IHandler<PutSongRequest, PutSongResponse> handler
        )
        {
            request.Id = Id;
            return handler.Handle(request);
        }

        [HttpDelete("{Id}")]
        public DeleteSongResponse Delete(
            [FromRoute] DeleteSongRequest request,
            [FromServices] IHandler<DeleteSongRequest, DeleteSongResponse> handler
        )
        {
            return handler.Handle(request);
        }
    }
}