﻿using Microsoft.Extensions.DependencyInjection;
using Repositories;

namespace Api
{
    public static class StartupRepositories
    {
        public static void SetupRepositories(this IServiceCollection services)
        {
            services.AddScoped<IArtistsRepository, ArtistsRepository>();
            services.AddScoped<ISongsRepository, SongsRepository>();
        }
    }
}