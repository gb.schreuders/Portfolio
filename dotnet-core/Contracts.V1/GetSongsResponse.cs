﻿using System;
using System.Collections.Generic;

namespace Contracts.V1
{
    public class GetSongsResponse
    {
        public IList<Song> Songs { get; set; }

        public class Song
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public int Year { get; set; }
            public string Artist { get; set; }
            public string Shortname { get; set; }
            public int Bpm { get; set; }
            public int Durration { get; set; }
            public string Genre { get; set; }
            public string SpotifyId { get; set; }
            public string Album { get; set; }
        }
    }
}