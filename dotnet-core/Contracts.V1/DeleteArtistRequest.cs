﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Contracts.V1
{
    public class DeleteArtistRequest
    {
        [Required]
        public Guid Id { get; set; }
    }
}