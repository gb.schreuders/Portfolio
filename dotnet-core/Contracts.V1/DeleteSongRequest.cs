﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Contracts.V1
{
    public class DeleteSongRequest
    {
        [Required] public Guid Id { get; set; }
    }
}