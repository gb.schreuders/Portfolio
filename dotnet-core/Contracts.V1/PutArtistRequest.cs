﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Contracts.V1
{
    public class PutArtistRequest
    {
        [Required] [IgnoreDataMember] public Guid Id { get; set; }

        [Required] public string Name { get; set; }
    }
}