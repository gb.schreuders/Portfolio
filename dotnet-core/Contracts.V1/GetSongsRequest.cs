﻿using System.ComponentModel.DataAnnotations;

namespace Contracts.V1
{
    public class GetSongsRequest
    {
        [Required]
        public string Genre { get; set; }
    }
}