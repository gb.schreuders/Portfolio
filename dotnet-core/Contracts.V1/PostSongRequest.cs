﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Contracts.V1
{
    public class PostSongRequest
    {
        [Required] [IgnoreDataMember] public string Name { get; set; }

        [Required] public int Year { get; set; }

        [Required] public string ArtistName { get; set; }

        [Required] public string Shortname { get; set; }

        [Required] public int Bpm { get; set; }

        [Required] public int Durration { get; set; }

        [Required] public string Genre { get; set; }

        [Required] public string SpotifyId { get; set; }

        [Required] public string Album { get; set; }
    }
}