﻿using System;
using System.Collections.Generic;

namespace Contracts.V1
{
    public class GetArtistsResponse
    {
        public IList<Artist> Artists { get; set; }

        public class Artist
        {
            public Guid Id { get; set; }

            public string Name { get; set; }
        }
    }
}