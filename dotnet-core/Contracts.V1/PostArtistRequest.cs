﻿using System.ComponentModel.DataAnnotations;

namespace Contracts.V1
{
    public class PostArtistRequest
    {
        [Required] public string Name { get; set; }
    }
}