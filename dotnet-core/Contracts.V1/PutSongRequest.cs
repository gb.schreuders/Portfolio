﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Contracts.V1
{
    public class PutSongRequest
    {
        [Required]
        [IgnoreDataMember]
        public Guid Id { get; set; }

        public string Name { get; set; }

        public int Year { get; set; }

        public string ArtistName { get; set; }

        public string Shortname { get; set; }

        public int Bpm { get; set; }

        public int Durration { get; set; }

        public string Genre { get; set; }

        public string SpotifyId { get; set; }

        public string AlbumName { get; set; }
    }
}