﻿using System;
using Contracts.V1;
using Repositories;

namespace Handlers
{
    public class PutArtistHandler : IHandler<PutArtistRequest, PutArtistResponse>
    {
        private readonly IArtistsRepository _artistsRepository;

        public PutArtistHandler(IArtistsRepository artistsRepository)
        {
            _artistsRepository = artistsRepository;
        }

        public PutArtistResponse Handle(PutArtistRequest request)
        {
            var artist = _artistsRepository.GetArtist(request.Id);

            if (artist == null)
            {
                throw new ArgumentException($"Artist with id '{request.Id}' doesn't exist.");
            }

            if (_artistsRepository.GetArtist(request.Name) != null)
            {
                throw new ArgumentException($"Artist with name '{request.Name}' already exists.");
            }

            artist.Name = request.Name;
            _artistsRepository.UpdateArtist(artist);

            return new PutArtistResponse();
        }
    }
}