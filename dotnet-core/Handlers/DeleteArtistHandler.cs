﻿using System;
using Contracts.V1;
using Repositories;

namespace Handlers
{
    public class DeleteArtistHandler : IHandler<DeleteArtistRequest, DeleteArtistResponse>
    {
        private readonly IArtistsRepository _artistsRepository;

        public DeleteArtistHandler(IArtistsRepository artistsRepository)
        {
            _artistsRepository = artistsRepository;
        }

        public DeleteArtistResponse Handle(DeleteArtistRequest request)
        {
            var artist = _artistsRepository.GetArtist(request.Id);

            if (artist == null)
            {
                throw new ArgumentException($"Artist with id '{request.Id}' doesn't exist.");
            }

            _artistsRepository.DeleteArtist(artist);

            return new DeleteArtistResponse();
        }
    }
}