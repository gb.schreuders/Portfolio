﻿using System;
using Contracts.V1;
using Repositories;

namespace Handlers
{
    public class PostArtistHandler : IHandler<PostArtistRequest, PostArtistResponse>
    {
        private readonly IArtistsRepository _artists;

        public PostArtistHandler(IArtistsRepository artists)
        {
            _artists = artists;
        }

        public PostArtistResponse Handle(PostArtistRequest request)
        {
            var artistName = request.Name;
            var artist = _artists.GetArtist(artistName);

            if (artist != null)
            {
                throw new ArgumentException($"Artist '{request.Name}' already exists.");
            }

            _artists.AddArtist(artistName);

            return new PostArtistResponse();
        }
    }
}