﻿using System;
using Contracts.V1;
using Entities;
using Repositories;

namespace Handlers
{
    public class PostSongHandler : IHandler<PostSongRequest, PostSongResponse>
    {
        private readonly ISongsRepository _songsRepository;
        private readonly IArtistsRepository _artistsRepository;

        public PostSongHandler(ISongsRepository songsRepository, IArtistsRepository artistsRepository)
        {
            _songsRepository = songsRepository;
            _artistsRepository = artistsRepository;
        }

        public PostSongResponse Handle(PostSongRequest request)
        {
            if (_songsRepository.GetSong(request.Name) != null)
            {
                throw new ArgumentException($"Song '{request.Name}' already exists.");
            }

            var artist = _artistsRepository.GetArtist(request.ArtistName);

            if (artist == null)
            {
                throw new ArgumentException($"Artist '{request.ArtistName}' doesn't exists.");
            }

            _songsRepository.AddSong(new Song
            {
                Name = request.Name,
                Artist = artist,
                Album = request.Album,
                Bpm = request.Bpm,
                Durration = request.Durration,
                Genre = request.Genre,
                Shortname = request.Shortname,
                SpotifyId = request.SpotifyId,
                Year = request.Year
            });

            return new PostSongResponse();
        }
    }
}