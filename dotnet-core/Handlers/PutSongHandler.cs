﻿using System;
using Contracts.V1;
using Entities;
using Repositories;
using static System.String;

namespace Handlers
{
    public class PutSongHandler : IHandler<PutSongRequest, PutSongResponse>
    {
        private readonly ISongsRepository _songsRepository;
        private readonly IArtistsRepository _artistsRepository;

        public PutSongHandler(ISongsRepository songsRepository, IArtistsRepository artistsRepository)
        {
            _songsRepository = songsRepository;
            _artistsRepository = artistsRepository;
        }

        public PutSongResponse Handle(PutSongRequest request)
        {
            var song = _songsRepository.GetSong(request.Id);

            if (song == null)
            {
                throw new ArgumentException($"Song with '{request.Id}' doesn't exist.");
            }

            CheckAndSetName(request, song);
            CheckAndSetArtist(request, song);
            song.Album = IsNullOrWhiteSpace(request.AlbumName) ? song.Album : request.AlbumName;
            song.Shortname = IsNullOrWhiteSpace(request.Shortname) ? song.Shortname : request.Shortname;
            song.SpotifyId = IsNullOrWhiteSpace(request.SpotifyId) ? song.SpotifyId : request.SpotifyId;
            song.Genre = IsNullOrWhiteSpace(request.Genre) ? song.Genre : request.Genre;
            song.Durration = request.Durration;
            song.Bpm = request.Bpm;
            song.Year = request.Year;

            _songsRepository.UpdateSong(song);

            return new PutSongResponse();
        }

        private void CheckAndSetArtist(PutSongRequest request, Song song)
        {
            if (IsNullOrWhiteSpace(request.ArtistName) || request.ArtistName == song.Artist.Name)
            {
                return;
            }

            var artist = _artistsRepository.GetArtist(request.ArtistName);
            if (artist == null)
            {
                throw new ArgumentException($"Artist '{request.ArtistName}' doesn't exist.");
            }

            song.Artist = artist;
        }

        private void CheckAndSetName(PutSongRequest request, Song song)
        {
            if (IsNullOrWhiteSpace(request.Name) || request.Name == song.Name)
            {
                return;
            }

            var songExists = _songsRepository.GetSong(request.Name) != null;
            if (songExists)
            {
                throw new ArgumentException($"Song with '{request.Name}' already exists.");
            }

            song.Name = request.Name;
        }
    }
}