﻿using System;
using Contracts.V1;
using Repositories;

namespace Handlers
{
    public class DeleteSongHandler : IHandler<DeleteSongRequest, DeleteSongResponse>
    {
        private readonly ISongsRepository _songsRepository;

        public DeleteSongHandler(ISongsRepository songsRepository)
        {
            _songsRepository = songsRepository;
        }

        public DeleteSongResponse Handle(DeleteSongRequest request)
        {
            var song = _songsRepository.GetSong(request.Id);

            if (song == null)
            {
                throw new ArgumentException($"Song with id '{request.Id}' doesn't exist.");
            }

            _songsRepository.DeleteSong(song);

            return new DeleteSongResponse();
        }
    }
}