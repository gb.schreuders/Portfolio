﻿using System;
using System.Linq;
using Contracts.V1;
using Repositories;

namespace Handlers
{
    public class GetSongsHandler : IHandler<GetSongsRequest, GetSongsResponse>
    {
        private readonly ISongsRepository _songsRepository;

        public GetSongsHandler(ISongsRepository songsRepository)
        {
            _songsRepository = songsRepository;
        }

        public GetSongsResponse Handle(GetSongsRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Genre))
            {
                throw new ArgumentException("Genre cannot be null or white space.");
            }

            var songs = _songsRepository.GetSongsByGenre(request.Genre);

            return new GetSongsResponse
            {
                Songs = songs.Select(song => new GetSongsResponse.Song
                {
                    Name = song.Name,
                    Artist = song.Artist.Name,
                    Genre = song.Genre,
                    Bpm = song.Bpm.GetValueOrDefault(),
                    SpotifyId = song.SpotifyId,
                    Shortname = song.Shortname,
                    Durration = song.Durration,
                    Album = song.Album,
                    Year = song.Year,
                    Id = song.SongId
                }).ToList()
            };
        }
    }
}