﻿using System.Linq;
using Contracts.V1;
using Repositories;

namespace Handlers
{
    public class GetArtistsHandler : IHandler<GetArtistsRequest, GetArtistsResponse>
    {
        private readonly IArtistsRepository _artistsRepository;

        public GetArtistsHandler(IArtistsRepository artistsRepository)
        {
            this._artistsRepository = artistsRepository;
        }

        public GetArtistsResponse Handle(GetArtistsRequest request)
        {
            var artists = _artistsRepository.GetArtists();

            return new GetArtistsResponse
            {
                Artists = artists.Select(a => new GetArtistsResponse.Artist
                {
                    Id = a.ArtistId,
                    Name = a.Name
                }).ToList()
            };
        }
    }
}