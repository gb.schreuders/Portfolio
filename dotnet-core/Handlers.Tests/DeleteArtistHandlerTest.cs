using System;
using System.Collections.Generic;
using Contracts.V1;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Repositories;

namespace Handlers.Tests
{
    [TestClass]
    public class DeleteArtistHandlerTest
    {
        [TestMethod]
        public void Should_delete_the_artist()
        {
            var artistRepository = new Mock<IArtistsRepository>();
            var handler = new DeleteArtistHandler(artistRepository.Object);

            var guid = Guid.NewGuid();
            var expectedArtist = new Artist {ArtistId = guid};
            Artist actualArtist = null;
            artistRepository.Setup(x => x.GetArtist(guid)).Returns(expectedArtist);
            artistRepository.Setup(x => x.DeleteArtist(expectedArtist)).Callback<Artist>(x => actualArtist = x);

            handler.Handle(new DeleteArtistRequest {Id = guid});

            Assert.AreEqual(expectedArtist.ArtistId, actualArtist.ArtistId);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_throw_error_with_non_existing_artist()
        {
            var artistRepository = new Mock<IArtistsRepository>();
            var handler = new DeleteArtistHandler(artistRepository.Object);

            var guid = Guid.NewGuid();
            artistRepository.Setup(x => x.GetArtist(guid)).Returns<Artist>(null);

            handler.Handle(new DeleteArtistRequest {Id = guid});
        }
    }
}