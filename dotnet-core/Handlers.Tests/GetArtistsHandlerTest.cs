using System;
using System.Collections.Generic;
using Contracts.V1;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Repositories;

namespace Handlers.Tests
{
    [TestClass]
    public class GetArtistsHandlerTest
    {
        [TestMethod]
        public void Should_get_all_artists()
        {
            var artistRepository = new Mock<IArtistsRepository>();
            var handler = new GetArtistsHandler(artistRepository.Object);

            artistRepository.Setup(x => x.GetArtists()).Returns(
                new List<Artist>
                {
                    new Artist {ArtistId = Guid.NewGuid()},
                    new Artist {ArtistId = Guid.NewGuid()},
                    new Artist {ArtistId = Guid.NewGuid()},
                }
            );

            var result = handler.Handle(new GetArtistsRequest());
            Assert.AreEqual(3, result.Artists.Count);
        }
    }
}