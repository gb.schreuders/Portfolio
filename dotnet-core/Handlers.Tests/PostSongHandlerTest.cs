using System;
using Contracts.V1;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Repositories;

namespace Handlers.Tests
{
    [TestClass]
    public class PostSongHandlerTest
    {
        [TestMethod]
        public void Should_work_with_correct_data()
        {
            var songsRepository = new Mock<ISongsRepository>();
            var artistRepository = new Mock<IArtistsRepository>();
            var handler = new PostSongHandler(songsRepository.Object, artistRepository.Object);

            Song song = null;
            var newGuid = Guid.NewGuid();

            songsRepository.Setup(x => x.GetSong(It.IsAny<string>())).Returns<Song>(null);
            artistRepository.Setup(x => x.GetArtist(It.IsAny<string>()))
                .Returns(new Artist {ArtistId = newGuid});
            songsRepository.Setup(x => x.AddSong(It.IsAny<Song>())).Callback<Song>(s => song = s);

            handler.Handle(new PostSongRequest
            {
                Name = "test",
                Album = "test",
                ArtistName = "test",
                Bpm = 0,
                SpotifyId = "test",
                Shortname = "test",
                Year = 0,
                Genre = "test",
                Durration = 0
            });

            Assert.AreEqual(song.Name, "test");
            Assert.AreEqual(song.Album, "test");
            Assert.AreEqual(song.Artist.ArtistId, newGuid);
            Assert.AreEqual(song.SpotifyId, "test");
            Assert.AreEqual(song.Name, "test");
            Assert.AreEqual(song.Genre, "test");
            Assert.AreEqual(song.Bpm, 0);
            Assert.AreEqual(song.Year, 0);
            Assert.AreEqual(song.Durration, 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_throw_on_existing_song()
        {
            var songsRepository = new Mock<ISongsRepository>();
            var artistRepository = new Mock<IArtistsRepository>();
            var handler = new PostSongHandler(songsRepository.Object, artistRepository.Object);

            songsRepository.Setup(x => x.GetSong(It.IsAny<string>())).Returns<Song>(null);

            handler.Handle(new PostSongRequest
            {
                Name = "test",
                Album = "test",
                ArtistName = "test",
                Bpm = 0,
                SpotifyId = "test",
                Shortname = "test",
                Year = 0,
                Genre = "test",
                Durration = 0
            });
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_throw_on_unknown_artist()
        {
            var songsRepository = new Mock<ISongsRepository>();
            var artistRepository = new Mock<IArtistsRepository>();
            var handler = new PostSongHandler(songsRepository.Object, artistRepository.Object);

            songsRepository.Setup(x => x.GetSong(It.IsAny<string>())).Returns(new Song());
            artistRepository.Setup(x => x.GetArtist(It.IsAny<Guid>())).Returns<Artist>(null);

            handler.Handle(new PostSongRequest
            {
                Name = "test",
                Album = "test",
                ArtistName = "test",
                Bpm = 0,
                SpotifyId = "test",
                Shortname = "test",
                Year = 0,
                Genre = "test",
                Durration = 0
            });
        }
    }
}