import { Component, Input, OnInit } from '@angular/core';
import { CardInfoDto } from '../../core/card-dto/card-info.dto';

@Component({
  selector: 'app-card-summary',
  templateUrl: './card-summary.component.html',
  styleUrls: ['./card-summary.component.scss']
})
export class CardSummaryComponent implements OnInit {
  @Input()
  cardInfo: CardInfoDto;

  constructor() {}

  ngOnInit() {}
}
