import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { CardSummaryComponent } from './card-summary.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CardInfoDto } from '../../core/card-dto/card-info.dto';

describe('CardSummaryComponent', () => {
  let component: CardSummaryComponent;
  let fixture: ComponentFixture<CardSummaryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CardSummaryComponent],
      imports: [RouterTestingModule.withRoutes([])]
    }).compileComponents();

    fixture = TestBed.createComponent(CardSummaryComponent);
    component = fixture.componentInstance;
  });

  it('should render the correct link', fakeAsync(() => {
    component.cardInfo = <CardInfoDto>{ name: 'test' };
    fixture.detectChanges();

    const openDetails: HTMLInputElement = fixture.nativeElement.querySelector(
      '#card-summary__open-details'
    );

    expect(openDetails.getAttribute('href')).toEqual('/details/test');
  }));
});
