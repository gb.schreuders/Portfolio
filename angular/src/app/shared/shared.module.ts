import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardSummaryComponent } from './card-summary/card-summary.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [CardSummaryComponent],
  exports: [CardSummaryComponent]
})
export class SharedModule {}
