import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { CardListComponent } from './card-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { CardStoreService } from '../core/card-store/card-store.service';
import SpyObj = jasmine.SpyObj;
import { of } from 'rxjs';
import { CardInfoDto } from '../core/card-dto/card-info.dto';

describe('CardListComponent', () => {
  let component: CardListComponent;
  let fixture: ComponentFixture<CardListComponent>;
  let cardStore: SpyObj<CardStoreService>;

  beforeEach(() => {
    cardStore = jasmine.createSpyObj<CardStoreService>('CardStoreService', [
      'getCards$'
    ]);

    TestBed.configureTestingModule({
      declarations: [CardListComponent],
      imports: [
        ReactiveFormsModule,
        SharedModule,
        RouterTestingModule.withRoutes([])
      ],
      providers: [
        {
          provide: CardStoreService,
          useValue: cardStore
        }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(CardListComponent);
    component = fixture.componentInstance;
  });

  it('should contain the right amount of cards', fakeAsync(() => {
    cardStore.getCards$.and.returnValue(
      of([
        <CardInfoDto>{ name: 'card1' },
        <CardInfoDto>{ name: 'card2' },
        <CardInfoDto>{ name: 'card3' }
      ])
    );

    fixture.detectChanges();

    const cardsContainer: HTMLInputElement = fixture.nativeElement.querySelector(
      '#card-list__card-container'
    );

    expect(cardsContainer.childElementCount).toEqual(3);
  }));

  it('should contain the right amount of cards after a search', fakeAsync(() => {
    cardStore.getCards$.and.returnValue(
      of([
        <CardInfoDto>{ name: 'card1' },
        <CardInfoDto>{ name: 'card2' },
        <CardInfoDto>{ name: 'card3' }
      ])
    );

    fixture.detectChanges();

    const nameInput: HTMLInputElement = fixture.nativeElement.querySelector(
      '#card-list__search-input'
    );
    nameInput.value = '3';
    nameInput.dispatchEvent(new Event('input'));
    fixture.detectChanges();

    const cardsContainer: HTMLInputElement = fixture.nativeElement.querySelector(
      '#card-list__card-container'
    );

    expect(cardsContainer.childElementCount).toEqual(1);
  }));

  it('should display 20 cards at first', fakeAsync(() => {
    cardStore.getCards$.and.returnValue(
      of([
        <CardInfoDto>{ name: 'card1' },
        <CardInfoDto>{ name: 'card2' },
        <CardInfoDto>{ name: 'card3' },
        <CardInfoDto>{ name: 'card4' },
        <CardInfoDto>{ name: 'card5' },
        <CardInfoDto>{ name: 'card6' },
        <CardInfoDto>{ name: 'card7' },
        <CardInfoDto>{ name: 'card8' },
        <CardInfoDto>{ name: 'card9' },
        <CardInfoDto>{ name: 'card10' },
        <CardInfoDto>{ name: 'card11' },
        <CardInfoDto>{ name: 'card12' },
        <CardInfoDto>{ name: 'card13' },
        <CardInfoDto>{ name: 'card14' },
        <CardInfoDto>{ name: 'card15' },
        <CardInfoDto>{ name: 'card16' },
        <CardInfoDto>{ name: 'card17' },
        <CardInfoDto>{ name: 'card18' },
        <CardInfoDto>{ name: 'card19' },
        <CardInfoDto>{ name: 'card20' },
        <CardInfoDto>{ name: 'card21' }
      ])
    );

    fixture.detectChanges();

    const cardsContainer: HTMLInputElement = fixture.nativeElement.querySelector(
      '#card-list__card-container'
    );

    expect(cardsContainer.childElementCount).toEqual(20);
  }));

  it('should display 21 cards when asked', fakeAsync(() => {
    cardStore.getCards$.and.returnValue(
      of([
        <CardInfoDto>{ name: 'card1' },
        <CardInfoDto>{ name: 'card2' },
        <CardInfoDto>{ name: 'card3' },
        <CardInfoDto>{ name: 'card4' },
        <CardInfoDto>{ name: 'card5' },
        <CardInfoDto>{ name: 'card6' },
        <CardInfoDto>{ name: 'card7' },
        <CardInfoDto>{ name: 'card8' },
        <CardInfoDto>{ name: 'card9' },
        <CardInfoDto>{ name: 'card10' },
        <CardInfoDto>{ name: 'card11' },
        <CardInfoDto>{ name: 'card12' },
        <CardInfoDto>{ name: 'card13' },
        <CardInfoDto>{ name: 'card14' },
        <CardInfoDto>{ name: 'card15' },
        <CardInfoDto>{ name: 'card16' },
        <CardInfoDto>{ name: 'card17' },
        <CardInfoDto>{ name: 'card18' },
        <CardInfoDto>{ name: 'card19' },
        <CardInfoDto>{ name: 'card20' },
        <CardInfoDto>{ name: 'card21' }
      ])
    );

    fixture.detectChanges();

    const cardsContainer: HTMLInputElement = fixture.nativeElement.querySelector(
      '#card-list__card-container'
    );

    const showMoreAction: HTMLInputElement = fixture.nativeElement.querySelector(
      '#card-list__show-more-action'
    );
    showMoreAction.click();

    fixture.detectChanges();

    expect(cardsContainer.childElementCount).toEqual(21);
  }));
});
