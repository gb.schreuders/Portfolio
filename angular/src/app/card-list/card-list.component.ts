import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { combineLatest, fromEvent, Observable } from 'rxjs';
import { CardInfoDto } from '../core/card-dto/card-info.dto';
import { FormControl } from '@angular/forms';
import { CardStoreService } from '../core/card-store/card-store.service';
import { map, scan, startWith, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss']
})
export class CardListComponent implements OnInit {
  public filteredCards$: Observable<CardInfoDto[]> = null;
  public cardsToDisplay$: Observable<number> = null;
  public searchValue$: Observable<string> = null;

  private readonly searchValueControl = new FormControl();

  @ViewChild('button')
  private readonly button: ElementRef;

  constructor(private readonly cardStore: CardStoreService) {}

  ngOnInit(): void {
    this.searchValue$ = this.searchValueControl.valueChanges.pipe(
      startWith('')
    );

    this.cardsToDisplay$ = this.searchValue$.pipe(
      switchMap(() =>
        fromEvent(this.button.nativeElement, 'click').pipe(
          startWith(0),
          scan(count => count + 20, 0)
        )
      )
    );

    this.filteredCards$ = combineLatest(
      this.cardStore.getCards$(),
      this.searchValue$,
      this.cardsToDisplay$
    ).pipe(
      map(([cards, value, amount]) =>
        cards
          .filter(card => card.name.toLowerCase().includes(value.toLowerCase()))
          .slice(0, amount)
      )
    );
  }
}
