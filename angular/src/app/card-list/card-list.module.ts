import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardListRoutingModule } from './card-list-routing.module';
import { CardListComponent } from './card-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    CardListRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [CardListComponent]
})
export class CardListModule {}
