import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/cards', pathMatch: 'full' },
  {
    path: 'details/:name',
    loadChildren: './card-details/card-details.module#CardDetailsModule'
  },
  {
    path: 'cards',
    loadChildren: './card-list/card-list.module#CardListModule'
  },
  { path: '**', redirectTo: '/not-found', pathMatch: 'full' },
  {
    path: 'not-found',
    loadChildren: './not-found/not-found.module#NotFoundModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
