import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardDetailsRoutingModule } from './card-details-routing.module';
import { CardDetailsComponent } from './card-details.component';

@NgModule({
  imports: [
    CommonModule,
    CardDetailsRoutingModule
  ],
  declarations: [CardDetailsComponent]
})
export class CardDetailsModule { }
