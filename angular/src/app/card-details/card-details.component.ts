import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CardInfoDto } from '../core/card-dto/card-info.dto';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { CardStoreService } from '../core/card-store/card-store.service';

@Component({
  selector: 'app-card-details',
  templateUrl: './card-details.component.html',
  styleUrls: ['./card-details.component.scss']
})
export class CardDetailsComponent implements OnInit {
  public cardDetails$: Observable<CardInfoDto> = null;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly cardStore: CardStoreService
  ) {}

  ngOnInit(): void {
    this.cardDetails$ = this.route.paramMap.pipe(
      switchMap(params =>
        this.cardStore
          .getCards$()
          .pipe(
            map(cards => cards.find(card => card.name === params.get('name')))
          )
      )
    );

    this.cardDetails$.subscribe(card => {
      if (card) {
        return;
      }

      this.router.navigate(['not-found']);
    });
  }
}
