import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { CardDetailsComponent } from './card-details.component';
import { CardStoreService } from '../core/card-store/card-store.service';
import { of } from 'rxjs';
import { CardInfoDto } from '../core/card-dto/card-info.dto';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import SpyObj = jasmine.SpyObj;
import { RouterTestingModule } from '@angular/router/testing';

describe('CardDetailsComponent', () => {
  let component: CardDetailsComponent;
  let fixture: ComponentFixture<CardDetailsComponent>;
  let cardStore: SpyObj<CardStoreService>;

  beforeEach(() => {
    cardStore = jasmine.createSpyObj<CardStoreService>('CardStoreService', [
      'getCards$'
    ]);

    TestBed.configureTestingModule({
      declarations: [CardDetailsComponent],
      imports: [RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: CardStoreService,
          useValue: cardStore
        },
        {
          provide: ActivatedRoute,
          useValue: { paramMap: of(convertToParamMap({ name: 'test-name' })) }
        }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(CardDetailsComponent);
    component = fixture.componentInstance;
  });

  it('should generate the correct name DOM data', fakeAsync(() => {
    cardStore.getCards$.and.returnValue(
      of([<CardInfoDto>{ name: 'test-name', cardId: 'test-cardId' }])
    );

    fixture.detectChanges();

    const nameElement = fixture.nativeElement.querySelector(
      '#card-details__name-value'
    );
    expect(nameElement.textContent).toEqual('test-name');
  }));

  it('should generate the correct id DOM data', fakeAsync(() => {
    cardStore.getCards$.and.returnValue(
      of([<CardInfoDto>{ name: 'test-name', cardId: 'test-cardId' }])
    );

    fixture.detectChanges();

    const idElement = fixture.nativeElement.querySelector(
      '#card-details__id-value'
    );
    expect(idElement.textContent).toEqual('test-cardId');
  }));

  it('should redirect to not-found on unknown card', fakeAsync(() => {
    const navigateSpy = spyOn((<any>component).router, 'navigate');

    cardStore.getCards$.and.returnValue(
      of([<CardInfoDto>{ name: 'incorrect', cardId: 'incorrect' }])
    );

    fixture.detectChanges();

    expect(navigateSpy).toHaveBeenCalledWith(['not-found']);
  }));
});
