import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetAllCardsResponseDto } from '../card-dto/get-all-cards-response.dto';

@Injectable({
  providedIn: 'root'
})
export class CardApiService {
  constructor(private readonly httpClient: HttpClient) {}

  public getCards(): Observable<GetAllCardsResponseDto> {
    return this.httpClient.get<GetAllCardsResponseDto>(
      './assets/all-cards.json'
    );
  }
}
