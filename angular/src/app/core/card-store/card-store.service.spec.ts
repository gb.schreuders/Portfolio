import { TestBed } from '@angular/core/testing';

import { CardStoreService } from './card-store.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('CardStoreService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    }));

  it('should be created', () => {
    const service: CardStoreService = TestBed.get(CardStoreService);
    expect(service).toBeTruthy();
  });
});
