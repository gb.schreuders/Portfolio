import { Injectable } from '@angular/core';
import { CardApiService } from '../card-api/card-api.service';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { CardInfoDto } from '../card-dto/card-info.dto';

@Injectable({
  providedIn: 'root'
})
export class CardStoreService {
  private _cards$: Observable<CardInfoDto[]> = null;

  constructor(private readonly cardApiService: CardApiService) {}

  public getCards$(): Observable<CardInfoDto[]> {
    if (this._cards$ === null) {
      this.initCards();
    }

    return this._cards$;
  }

  private initCards(): void {
    this._cards$ = this.cardApiService.getCards().pipe(
      take(1),
      map(response => {
        const cards = [];
        for (const key in response) {
          cards.push(...response[key]);
        }
        return cards;
      }),
      map((cards: CardInfoDto[]) => cards.filter(card => card.collectible)),
      map((x: CardInfoDto[]) => x.filter(card => card.type !== 'Hero'))
    );
  }
}
