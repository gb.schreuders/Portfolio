import { CardInfoDto } from './card-info.dto';

export interface GetAllCardsResponseDto {
  [expansion: string]: CardInfoDto[];
}
