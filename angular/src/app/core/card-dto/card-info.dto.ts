export interface CardInfoDto {
  cardId: string;
  dbfId: number;
  name: string;
  type: string;
  faction: string;
  rarity: string;
  cost: number;
  attack: number;
  health: number;
  durability: number;
  armor: number;
  text: string;
  inPlayText: string;
  flavor: string;
  artist: string;
  collectible: boolean;
  elite: string;
  race: string;
  playerClass: string;
  multiClassGroup: string;
  classes: string;
  howToGet: string;
  howToGetGold: string;
  img: string;
  imgGold: string;
  locale: string;
  mechanics: { name: string }[];
}
