import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardStoreService } from './card-store/card-store.service';
import { HttpClientModule } from '@angular/common/http';
import { CardApiService } from './card-api/card-api.service';

@NgModule({
  imports: [CommonModule, HttpClientModule],
  providers: [CardStoreService, CardApiService],
  declarations: []
})
export class CoreModule {}
