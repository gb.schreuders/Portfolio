# Introduction

Welcome to my portfolio repository. The purpose of this repository is to give an indication of my skills.

This portfolio contains the following projects:

| Project dir | Context | 
| ----------- | ------- |
| [angular](https://gitlab.com/gb.schreuders/Portfolio/tree/master/angular) | A card browser SPA for the populair card game Hearthstone. |
| [react](https://gitlab.com/gb.schreuders/Portfolio/tree/master/react) | A champion browser for the populair MOBA game League of Legends. |
| [dotnet-core](https://gitlab.com/gb.schreuders/Portfolio/tree/master/dotnet-core) | A simple CRUD API for music. |

# CI (continuous integration)

Both projects have build automation which can be found in the `.gitlab-ci.yml` file.


